FROM python:3-alpine

LABEL maintainer="benoit.pourre@gmail.com"
LABEL repository=https://gitlab.com

WORKDIR /usr/src/app
COPY --chown=root:root watchtower.py /usr/src/app/watchtower.py

RUN pip3 install --no-cache-dir -r /builds/captnbp/k8s-watchtower/requirements.txt

CMD ["python3", "/usr/src/app/watchtower.py"]

USER nobody