from kubernetes import client, config
from pprint import pprint
from kubernetes.client.rest import ApiException
import docker
from logzero import logger
import urllib3.exceptions

try:
  namespace = open("/var/run/secrets/kubernetes.io/serviceaccount/namespace").read()
except FileNotFoundError as e:
  logger.warning("Can't find /var/run/secrets/kubernetes.io/serviceaccount/namespace. Are you running in a Kubernetes Pod ?")

pretty = 'true' # str | If 'true', then the output is pretty printed. (optional)
label_selector = '' # str | A selector to restrict the list of returned objects by their labels. Defaults to everything. (optional)
timeout_seconds = 56 # int | Timeout for the list/watch call. This limits the duration of the call, regardless of any activity or inactivity. (optional)

# Configs can be set in Configuration class directly or using helper utility
try:
  config.load_incluster_config()
except config.config_exception.ConfigException as e:
  logger.warning("Can't find serviceAccount config. Not running in a Pod")
  logger.info("Trying to load KUBECONFIG")
  try:
    config.load_kube_config()
    namespace = config.list_kube_config_contexts()[1]['context']['namespace']
    namespace = "kube-system"
  except Exception as e:
    logger.error("No KUBECONFIG found. Can't talk to the API. Exiting.")
    exit(1)
app = client.AppsV1Api()

try:
  client = docker.from_env(version='auto')
except docker.errors.DockerException as e:
  logger.error("Failed to connect to Docker daemon API: %s" % (e))
  logger.error("Please mount /var/run/docker.sock:ro or provide environment variables as documented here : https://docker-py.readthedocs.io/en/stable/client.html#docker.client.from_env")
  logger.error("Exiting")
  exit(1)

logger.info("Listing deployments with their container images")
try:
  ret = app.list_namespaced_deployment(
    namespace, 
    pretty=pretty, 
    label_selector=label_selector, 
    timeout_seconds=timeout_seconds, 
    watch=False
  )
except ApiException as e:
  logger.error("Exception when calling AppsV1Api->list_namespaced_deployment: %s" % (e))
except urllib3.exceptions.MaxRetryError as e:
  logger.error("Failed to connect to Kubernetes API: %s" % (e))
  logger.info("Exiting")
  exit(1)
for deployment in ret.items:
  logger.info("Found deployment \"%s\" in namespace %s with %s container(s)" % 
    (deployment.metadata.name, deployment.metadata.namespace, len(deployment.spec.template.spec.containers)))
  container_index = 0
  patches = []
  for container in deployment.spec.template.spec.containers:
    logger.debug("%s: Container %s image name is \"%s\"" % 
      (deployment.metadata.name, container_index, container.image))
    image = container.image.split('@')
    try:
      in_use_image_name = image[0]
      in_use_image_id = image[1]
    except IndexError as e:
      in_use_image_name = container.image
      in_use_image_id = None
    registry_image_id = client.images.get_registry_data(in_use_image_name).id

    if in_use_image_id != registry_image_id:
      logger.info("%s: Container %s -> Need to upgrade image %s id from %s to %s" % 
        (deployment.metadata.name, container_index, in_use_image_name, in_use_image_id, registry_image_id))
      patches.append({"op": "replace", "value": "{}@{}".format(in_use_image_name, registry_image_id), "path": "/spec/template/spec/containers/{}/image".format(container_index)})
    else:
      logger.info("%s: Container %s -> Nothing to do for image %s id from %s to %s" % 
        (deployment.metadata.name, container_index, in_use_image_name, in_use_image_id, registry_image_id))

    container_index = container_index + 1
  try:
    app.patch_namespaced_deployment(name=deployment.metadata.name, namespace=deployment.metadata.namespace, body=patches)
  except Exception as e:
    logger.exception("%s: Failed to patch deployment:" % (deployment.metadata.name))

logger.info("End of watch for namespace %s" % (namespace))
exit(0)